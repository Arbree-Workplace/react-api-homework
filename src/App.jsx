import * as React from "react"
import useAuth from "./hooks/useAuth"

const AuthorizedApp = React.lazy(() => import('./components/App/AuthorizedApp'))

const UnauthorizedApp = React.lazy(() => import('./components/App/UnAuthorizedApp'))

const App = () => {
    const { token } = useAuth()

    return (
        <div id="App" className="relative container h-screen px-2 md:px-4">
            <React.Suspense fallback={<h1>Loading...</h1>}>
                {token ? <AuthorizedApp /> : <UnauthorizedApp />}
            </React.Suspense>
        </div>
    )
}

export default App