async function useFetchPost(endpoint, payload) {
    const token = localStorage.getItem('token')

    const djangoDrfLTokenLiteral = 'Token' + ' ' + token

    const config = {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Authorization': djangoDrfLTokenLiteral,
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    }

    const url = 'http://localhost:8000/api/'

    const api = url + endpoint + '/'

    const response = await fetch(api, config)

    return await response.json()
}

export default useFetchPost