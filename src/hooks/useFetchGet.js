async function useFetchGet(endpoint) {
    const token = localStorage.getItem('token')

    const djangoDrfLTokenLiteral = 'Token' + ' ' + token

    const config = {
        method: 'GET',
        mode: 'cors',
        headers: {
            'Authorization': djangoDrfLTokenLiteral,
            'Content-Type': 'application/json',
        }
    }

    const url = 'http://localhost:8000/api/'

    const api = url + endpoint + '/'

    const response = await fetch(api, config)

    return await response.json()
}

export default useFetchGet