import * as React from "react"
import Ingredients from "../components/Kitchen/Ingredients"
import Recipes from "../components/Kitchen/Recipes"
import Tags from "../components/Kitchen/Tags"

function Kitchen() {
    return (
        <div className="flex flex-col w-max rounded-lg mx-auto mt-10 bg-gray-200">
            <div className="flex flex-row">
                <Ingredients />
                <Tags />
                <Recipes />
            </div>
        </div>
    )
}

export default Kitchen