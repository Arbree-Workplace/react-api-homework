import * as React from "react"
import UserInfo from "../components/Auth/UserInfo"
import UserUpdateForm from "../components/Auth/UserUpdateForm"
import useFetchPut from '../hooks/useFetchPut'
import useFetchPatch from "../hooks/useFetchPatch"

function Profile() {
    const [userData, setUserData] = React.useState({})

    const [inputs, setInputs] = React.useState({})

    const handleInputChange = (event) => {
        const name = event.target.name
        const value = event.target.value
        setInputs((values) => ({ ...values, [name]: value }))
    }

    const handleUpdateProfile = async (event) => {
        event.preventDefault()
        let isAllvalid = true
        for (const val of Object.values(inputs)) {
            if (!val) {
                isAllvalid = false
                break
            }
        }
        const payload = inputs
        if (isAllvalid) {
            const data = await useFetchPut('accounts/me', payload)
            setUserData(data)
        } else {
            const data = await useFetchPatch('accounts/me', payload)
            setUserData(data)
        }
    }

    return (
        <div className="flex flex-col gap-4 py-2">
            <UserInfo
                userData={userData}
                setUserData={setUserData} />
            <UserUpdateForm
                inputs={inputs}
                handleInputChange={handleInputChange}
                handleUpdateProfile={handleUpdateProfile} />
        </div>
    )
}

export default Profile