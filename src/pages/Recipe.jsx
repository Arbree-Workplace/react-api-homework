import * as React from 'react'
import { useParams } from "react-router"
import useFetchGet from "../hooks/useFetchGet"
import useFetchPost from '../hooks/useFetchPost'
import useFetchPut from '../hooks/useFetchPut'
import useFetchPatch from '../hooks/useFetchPatch'
import useFetchDelete from '../hooks/useFetchDelete'

function Recipe() {
    const [recipeDetail, setRecipeDetail] = React.useState({
        ingredients: [{}],
        tags: [{}]
    })

    const [recipeImage, setRecipeImage] = React.useState(null)

    const [recipeEditInput, setRecipeEditInput] = React.useState({})

    const params = useParams()

    React.useEffect(async () => {
        const data = await useFetchGet('recipe/recipes/' + params.recipeID)
        setRecipeDetail(data)
    }, [])

    const handleRecipeEditInputs = (event) => {
        const name = event.target.name
        const value = event.target.value
        setRecipeEditInput(values => ({ ...values, [name]: value }))
    }

    const handleEditRecipe = async (event) => {
        event.preventDefault()
        recipeEditInput.ingredients = recipeEditInput.ingredients.split(',').map(item => parseInt(item, 10))
        recipeEditInput.tags = recipeEditInput.tags.split(',').map(item => parseInt(item, 10))
        let isAllvalid = true
        for (const val of Object.values(recipeEditInput)) {
            if (!val) {
                isAllvalid = false
                break
            }
        }
        const payload = recipeEditInput
        if (isAllvalid) {
            const data = await useFetchPut('recipe/recipes/' + params.recipeID, payload)
            console.log(data)
            setRecipeDetail(data)
        } else {
            const data = await useFetchPatch('recipe/recipes/' + params.recipeID, payload)
            console.log(data)
            setRecipeDetail(data)
        }
    }

    const handleDeleteRecipe = async (event) => {
        event.preventDefault()
        const id = event.target.id
        const payload = { id }
        const data = await useFetchDelete('recipe/recipes/' + params.recipeID, payload)
        console.log(data)
    }

    const handleFileInputChange = (event) => {
        const imageFile = event.target.value
        setRecipeImage(imageFile)
    }

    const handleUploadImage = async (event) => {
        event.preventDefault()
        console.log(recipeImage)
        const payload = { id: params.recipeID, recipeImage }
        await useFetchPost(`recipe/recipes/${params.recipeID}/recipe-upload-image`, payload)
    }

    return (
        <div className="w-max flex flex-row gap-10 p-2">
            <div className="flex flex-col gap-2">
                <div>Title: {recipeDetail.title}</div>
                <div>Time in minutes: {recipeDetail.time_minutes}</div>
                <div>Price: {recipeDetail.price}</div>
                <div className="flex flex-row gap-2" >
                    <span>Ingredients:</span>
                    {recipeDetail.ingredients.map((item, idx) =>
                        <div key={idx}>
                            {item.name}
                        </div>
                    )}
                </div>
                <div className="flex flex-row gap-2" >
                    <span>Tags:</span>
                    {recipeDetail.tags.map((item, idx) =>
                        <div key={idx}>
                            {item.name}
                        </div>
                    )}
                </div>
                <button
                    id={recipeDetail.id}
                    className="px-2 py-1 rounded bg-red-600 text-white"
                    onClick={(event) => handleDeleteRecipe(event)}>
                    Delete
                </button>
            </div>

            <form className="recipe-edit-form flex flex-col gap-2">
                <input
                    className="px-2 py-1 border-2 border-gray-600 outline-none"
                    onChange={(event) => handleRecipeEditInputs(event)}
                    type="text"
                    name="title"
                    value={recipeEditInput.title || ''}
                    placeholder="Title"
                />
                <input
                    className="px-2 py-1 border-2 border-gray-600 outline-none"
                    onChange={(event) => handleRecipeEditInputs(event)}
                    type="text"
                    name="ingredients"
                    value={recipeEditInput.ingredients || ''}
                    placeholder="Ingredients (eg. 3 5 6)"
                />
                <input
                    className="px-2 py-1 border-2 border-gray-600 outline-none"
                    onChange={(event) => handleRecipeEditInputs(event)}
                    type="text"
                    name="tags"
                    value={recipeEditInput.tags || ''}
                    placeholder="Tags (eg. 1 2 3)"
                />
                <input
                    className="px-2 py-1 border-2 border-gray-600 outline-none"
                    onChange={(event) => handleRecipeEditInputs(event)}
                    type="text"
                    name="time_minutes"
                    value={recipeEditInput.time_minutes || ''}
                    placeholder="Time in Minutes"
                />
                <input
                    className="px-2 py-1 border-2 border-gray-600 outline-none"
                    onChange={(event) => handleRecipeEditInputs(event)}
                    type="text"
                    name="price"
                    value={recipeEditInput.price || ''}
                    placeholder="Price"
                />
                <button
                    className="px-2 py-1 rounded bg-blue-700 text-gray-50"
                    onClick={(event) => handleEditRecipe(event)}>
                    Edit Recipe
                </button>
            </form>
            <form
                className="flex flex-col gap-2"
                onSubmit={(event) => handleUploadImage(event)}>
                <header className="text-2xl">Upload An Image!</header>
                <input
                    className="cursor-pointer"
                    name="image"
                    type="file"
                    accept="image/*"
                    value={recipeImage || ''}
                    onChange={(event) => handleFileInputChange(event)} />
                <button
                    className="px-2 py-1 rounded bg-green-600 text-white"
                    type="submit">Submit</button>
            </form>
        </div>
    )
}

export default Recipe