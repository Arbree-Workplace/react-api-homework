import * as React from 'react'
import Navbar from '../Utils/Navbar'
import AppRoutes from './AppRoutes'

function AuthorizedApp() {
    return (
        <div className="mt-4">
            <Navbar />
            <AppRoutes />
        </div>
    )
}

export default AuthorizedApp