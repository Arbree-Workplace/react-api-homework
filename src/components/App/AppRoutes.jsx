import { Routes, Route } from 'react-router-dom'
import Profile from '../../pages/Profile'
import Kitchen from '../../pages/Kitchen'
import Recipe from '../../pages/Recipe'

function AppRoutes() {
    return (
        <Routes>
            <Route path="/" element={<Profile />} />
            <Route path="kitchen" element={<Kitchen />} />
            <Route path="kitchen/recipe/:recipeID" element={<Recipe />} />
        </Routes>
    )
}

export default AppRoutes