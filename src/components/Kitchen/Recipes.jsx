import * as React from 'react'
import useFetchGet from '../../hooks/useFetchGet'
import useFetchPost from '../../hooks/useFetchPost'
import { Link } from 'react-router-dom'

function Recipes() {
    const [recipes, setRecipes] = React.useState([])

    const [recipeInput, setRecipeInput] = React.useState({})

    React.useEffect(async () => {
        const data = await useFetchGet('recipe/recipes')
        setRecipes(data)
    }, [])

    const handleRecipeInputs = (event) => {
        const name = event.target.name
        const value = event.target.value
        setRecipeInput(values => ({ ...values, [name]: value }))
    }

    const handleCreateRecipe = async (event) => {
        event.preventDefault()
        recipeInput.ingredients = recipeInput.ingredients.split(',').map(item => parseInt(item, 10))
        recipeInput.tags = recipeInput.tags.split(',').map(item => parseInt(item, 10))
        const payload = recipeInput
        const data = await useFetchPost('recipe/recipes', payload)
        setRecipes([...recipes, data])
    }

    return (
        <div className="p-3 flex flex-row gap-2">
            <form className="flex flex-col gap-2">
                <input
                    className="px-2 py-1 border-2 border-gray-600 outline-none"
                    onChange={(event) => handleRecipeInputs(event)}
                    type="text"
                    name="title"
                    value={recipeInput.title || ''}
                    placeholder="Title"
                />
                <input
                    className="px-2 py-1 border-2 border-gray-600 outline-none"
                    onChange={(event) => handleRecipeInputs(event)}
                    type="text"
                    name="ingredients"
                    value={recipeInput.ingredients || ''}
                    placeholder="Ingredients (eg. 1,2,3)"
                />
                <input
                    className="px-2 py-1 border-2 border-gray-600 outline-none"
                    onChange={(event) => handleRecipeInputs(event)}
                    type="text"
                    name="tags"
                    value={recipeInput.tags || ''}
                    placeholder="Tags (eg. 1,2,3)"
                />
                <input
                    className="px-2 py-1 border-2 border-gray-600 outline-none"
                    onChange={(event) => handleRecipeInputs(event)}
                    type="text"
                    name="time_minutes"
                    value={recipeInput.time_minutes || ''}
                    placeholder="Time in Minutes"
                />
                <input
                    className="px-2 py-1 border-2 border-gray-600 outline-none"
                    onChange={(event) => handleRecipeInputs(event)}
                    type="text"
                    name="price"
                    value={recipeInput.price || ''}
                    placeholder="Price"
                />
                <button
                    className="px-2 py-1 rounded bg-blue-700 text-gray-50"
                    onClick={(event) => handleCreateRecipe(event)}>
                    Create Recipe
                </button>
            </form>
            <div className="flex flex-col gap-2">
                {recipes.map((item, idx) =>
                    <Link to={"/kitchen/recipe/" + item.id}>
                        <div
                            key={idx}
                            className="flex flex-row gap-2 px-1 py-2 border-2 rounded border-gray-600" >
                            <div className="px-2 py-1 rounded-md bg-blue-700 text-white">
                                View
                            </div>
                            <div>{item.title}</div>
                        </div>
                    </Link>
                )}
            </div>
        </div>
    )
}

export default Recipes