import * as React from 'react'
import useFetchGet from '../../hooks/useFetchGet'
import useFetchPost from '../../hooks/useFetchPost'

function Tags() {
    const [tags, setTags] = React.useState([])

    const [tagInput, setTagInput] = React.useState(null)

    React.useEffect(async () => {
        const data = await useFetchGet('recipe/tags')
        setTags(data)
    }, [])

    const handleTagtInput = (event) => {
        const value = event.target.value
        setTagInput(value)
    }

    const handleCreateTag = async (event) => {
        event.preventDefault()
        const payload = { name: tagInput }
        const data = await useFetchPost('recipe/tags', payload)
        setTags([...tags, data])
    }

    return (
        <div className="w-max p-3">
            <div className="flex flex-col gap-2">
                {tags.map(item =>
                    <div
                        key={item.id}
                        className=" flex flex-row gap-2 p-2 border-2 rounded border-gray-600">
                        <div className="px-2 py-1 rounded-md bg-green-700 text-white">
                            {item.id}
                        </div>
                        <div>
                            {item.name}
                        </div>
                    </div>
                )}

            </div>
            <form className="flex flex-row py-2 gap-2">
                <input
                    className="px-2 py-1 border-2 border-gray-600 outline-none"
                    onChange={(event) => handleTagtInput(event)}
                    type="text"
                    name="name"
                    value={tagInput || ''}
                    placeholder="Write one!"
                />
                <button
                    className="px-2 py-1 rounded bg-blue-700 text-gray-50"
                    onClick={(event) => handleCreateTag(event)}>
                    Create Tag
                </button>
            </form>
        </div>
    )
}

export default Tags