import * as React from 'react'
import useFetchGet from '../../hooks/useFetchGet'
import useFetchPost from '../../hooks/useFetchPost'

function Ingredients() {
    const [ingredients, setIngredients] = React.useState([])

    const [ingredientInput, setIngredientInput] = React.useState(null)

    React.useEffect(async () => {
        const data = await useFetchGet('recipe/ingredients')
        setIngredients(data)
    }, [])

    const handleIngredientInputs = (event) => {
        const value = event.target.value
        setIngredientInput(value)
    }

    const handleCreateIngredient = async (event) => {
        event.preventDefault()
        const payload = { name: ingredientInput }
        const data = await useFetchPost('recipe/ingredients', payload)
        setIngredients([...ingredients, data])
    }

    return (
        <div className="w-max p-3">
            <div className="flex flex-col gap-2">

                {ingredients.map(item =>
                    <div
                        key={item.id}
                        className=" flex flex-row gap-2 p-2 border-2 rounded border-gray-600">
                        <div className="px-2 py-1 rounded-md bg-green-700 text-white">
                            {item.id}
                        </div>
                        <div className="">
                            {item.name}
                        </div>
                    </div>
                )}

            </div>
            <form className="create-ingredient-form flex flex-row py-2 gap-2">
                <input
                    className="px-2 py-1 border-2 border-gray-600 outline-none"
                    onChange={(event) => handleIngredientInputs(event)}
                    type="text"
                    name="name"
                    value={ingredientInput || ''}
                    placeholder="Write one!"
                />
                <button
                    className="px-2 py-1 rounded bg-blue-700 text-gray-50"
                    onClick={(event) => handleCreateIngredient(event)}>
                    Create Ingredient
                </button>
            </form>
        </div>
    )
}

export default Ingredients