import { React, useState } from "react"

function SignUpForm(props) {
    const [inputs, setInputs] = useState({})

    const handleChange = (event) => {
        const name = event.target.name
        const value = event.target.value
        setInputs(values => ({ ...values, [name]: value }))
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const api_call = 'http://localhost:8000/api/accounts/create/'

        const payload = inputs

        const config = {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(payload)
        }

        const response = await fetch(api_call, config)

        response.ok ? props.loginUponSignUp() : null

    }

    return (
        <form className="flex flex-col px-2 py-2 gap-4"
            onSubmit={(event) => handleSubmit(event)}>
            <input
                type="text"
                name="name"
                value={inputs.name || ''}
                placeholder="Username"
                className="px-2 py-1"
                onChange={(event) => handleChange(event)}
            />
            <input
                type="email"
                name="email"
                value={inputs.email || ''}
                placeholder="Email"
                className="px-2 py-1"
                onChange={(event) => handleChange(event)}
            />
            <input
                type="password"
                name="password"
                value={inputs.password || ""}
                placeholder="Password"
                className="px-2 py-1"
                onChange={(event) => handleChange(event)}
            />
            <input
                type="submit"
                className="px-2 py-2 rounded bg-indigo-300 cursor-pointer" />
        </form>
    )
}

export default SignUpForm