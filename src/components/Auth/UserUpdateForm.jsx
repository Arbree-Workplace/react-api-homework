function UserUpdateForm(props) {
    return (
        <form
            className="flex flex-col gap-2 w-min"
            onSubmit={(event) => props.handleUpdateProfile(event)}>
            <input
                type="text"
                name="name"
                value={props.inputs.name || ''}
                placeholder="Username"
                className="px-2 py-1 border-2 border-gray-600 outline-none"
                onChange={(event) => props.handleInputChange(event)}
            />
            <input
                type="email"
                name="email"
                value={props.inputs.email || ''}
                placeholder="Email"
                className="px-2 py-1 border-2 border-gray-600 outline-none"
                onChange={(event) => props.handleInputChange(event)}
            />
            <input
                type="password"
                name="password"
                value={props.inputs.password || ""}
                placeholder="Password"
                className="px-2 py-1 border-2 border-gray-600 outline-none"
                onChange={(event) => props.handleInputChange(event)}
            />
            <button
                className="px-1 py-2 rounded bg-green-600 text-white"
                type="submit">Update Profile</button>
        </form>
    )
}

export default UserUpdateForm