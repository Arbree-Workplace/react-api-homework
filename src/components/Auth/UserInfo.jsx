import * as React from "react";
import useFetchGet from '../../hooks/useFetchGet'

function UserInfo(props) {
    React.useEffect(async () => {
        const data = await useFetchGet('accounts/me')
        props.setUserData(data)
    }, [])

    return (
        <div className="my-profile flex flex-col gap-2">
            <header className="text-2xl">My Profile</header>
            <div>
                <div>Name: {props.userData.name} </div>
                <div>Email: {props.userData.email} </div>
            </div>
        </div>
    )
}

export default UserInfo