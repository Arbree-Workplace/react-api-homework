import * as React from "react"
import { AuthContext } from "../../contexts/AuthContext"

const getUser = () => localStorage.getItem('token')

function AuthProvider({ children }) {
    const userState = getUser()

    let [token, setToken] = React.useState(userState)

    const signin = (token) => {
        setToken(token)
        localStorage.setItem('token', token)
    }

    const signout = () => {
        setToken(null)
        localStorage.removeItem('token')
    }

    const value = { token, signin, signout }

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>
}

export default AuthProvider