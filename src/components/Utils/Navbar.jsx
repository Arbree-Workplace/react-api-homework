import { Link } from 'react-router-dom'
import useAuth from "../../hooks/useAuth"

function Navbar() {
    const { signout } = useAuth()

    return (
        <nav
            className="w-full flex flex-row justify-between p-3 rounded bg-indigo-200">
            <div className="flex flex-row gap-2">
                <Link to="/">
                    <div className="px-2 py-1 rounded bg-green-600 text-white">
                        Home
                    </div>
                </Link>
                <Link to="/kitchen">
                    <div className="px-2 py-1 rounded bg-indigo-600 text-white">
                        Kitchen
                    </div>
                </Link>
            </div>
            <button className="px-2 py-1 rounded bg-blue-600 text-gray-50"
                onClick={() => signout()}>
                Sign Out
            </button>
        </nav>
    )
}

export default Navbar